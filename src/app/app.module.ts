import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ConversationComponent } from './conversation/conversation.component';
import { ProfileComponent } from './profile/profile.component';
import { RouterModule } from '@angular/router';
import {Routes} from '@angular/router';
import { OptComponent } from './opt/opt.component'
import { SearchPipe } from './pipes/search';
import { FormsModule } from '@angular/forms';
import { MenuComponent } from './menu/menu.component';

import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AuthenticationGuard } from './Services/authentication.guard';
import { ImageCropperModule } from 'ngx-image-cropper';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { RequestComponent } from './modals/request/request.component';
import { ContactComponent } from './contact/contact.component';



const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component : HomeComponent, canActivate:[AuthenticationGuard]},
  {path: 'login', component : LoginComponent}, 
  {path: 'conversation/:id', component : ConversationComponent, canActivate:[AuthenticationGuard]}, 
  {path: 'profile', component : ProfileComponent , canActivate:[AuthenticationGuard]}, 
  {path: 'opt', component : OptComponent , canActivate:[AuthenticationGuard]}, 
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ConversationComponent,
    ProfileComponent,
    OptComponent,
    SearchPipe,
    MenuComponent,
    RequestComponent,
    ContactComponent    
  ],
  imports: [
    NgbModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    //AngularFireModule.initializeApp(environment.firebase),
    AngularFireModule.initializeApp(environment.firebaseConfig) , 
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireDatabaseModule,
    ImageCropperModule,
    BootstrapModalModule.forRoot({container: document.body})
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [RequestComponent]
})
export class AppModule { }
