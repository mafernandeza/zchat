import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {

  constructor(private angularFireDataBase: AngularFireDatabase) { }
  createConversation(conversation){
    return this.angularFireDataBase.object('conversation/'+conversation.id+'/'+conversation.timestamp).set(conversation);
  }
  getConversation(id){
    return this.angularFireDataBase.list('conversation/'+id);
  }
  editConversation(conversation){
    return this.angularFireDataBase.object('conversation/'+conversation.id+'/'+conversation.timestamp).set(conversation);
  }
}
