import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../Services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }
  loginOut(){
    this.authenticationService.logOut().then(()=>{
      this.router.navigate(['login']);
    },
     (error)=>{ alert('ocurrio un problema al cerrar sesión.'); console.log(error)} );
  }

}
