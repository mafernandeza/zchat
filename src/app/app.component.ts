import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './Services/authentication.service';
import { UserService } from './Services/user.service';
import { RequestsService } from './Services/requests.service';
import { user } from './Interfaces/user';
import { DialogService } from 'ng2-bootstrap-modal';
import { RequestComponent } from './modals/request/request.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'zahuriochat';
  user: user;
  requests: any[] = [];
  mailsShow: any[] = [];

  constructor (public router:Router, private autenticationService:  AuthenticationService, private userService: UserService,
    private requestService: RequestsService, private dialogService: DialogService){
      this.autenticationService.getStatus().subscribe(
        (status)=>{
          this.userService.getUserById(status.uid).valueChanges().subscribe(
            (data: user)=>{
              this.user = data; 
              this.requestService.getRequestsForEmail(this.user.email).valueChanges().subscribe(
                (data: any)=>{ 
                  this.requests = data; 
                  this.requests = this.requests.filter( 
                    (r)=>{ return r.status != 'accepted' && r.status != 'rejected';}
                  );
                  
                  this.requests.forEach( 
                    (r)=>{
                      if(this.mailsShow.indexOf(r.sender) === -1){
                        this.mailsShow.push((r.sender));
                        this.dialogService.addDialog(RequestComponent, {scope: this,currentRequest:r} )
                      }
                    }
                  )
                 },
                (error)=>{console.log(error); alert('ocurrio un error al obtener requests');}
              );
            } 
          );
        }
      );
    }
}
