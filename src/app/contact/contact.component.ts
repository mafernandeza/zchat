import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../Services/user.service';
import { user } from '../Interfaces/user';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  @Input() uid : string; 
  contact : user; 
  query : string = ""; 

  constructor(private userService: UserService) { }

  ngOnInit() {
    console.log(this.uid);
    this.userService.getUserById(this.uid).valueChanges().subscribe(
      (data :user)=>{
        this.contact = data;
      }
    ); 
  }

}
