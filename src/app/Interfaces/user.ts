export interface user {
    nick : string ;
    subNick? : string ;  // ? -->  permite que el campo sea opcional
    age? : number;
    email:  string ;
    friend? : boolean ;
    id : any;
    status?: string;
    avatar? : string;
    friends?: any;
}
