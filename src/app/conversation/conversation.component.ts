import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { user } from '../Interfaces/user';
import { UserService } from '../Services/user.service';
import { ConversationService } from '../Services/conversation.service';
import { AuthenticationService } from '../Services/authentication.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {
  //friendId  :  any ;
  friend : user = null;
  now : any  =  "null";
  user : user;
  conversation_id: string;
  textMessage : string ; 
  conversation : any []; 
  shake : boolean   = false; 

  croppedImage: any = '';
  imageChangedEvent: any = '';

  constructor( private activatedRoute : ActivatedRoute,
               private userService : UserService,private conversationService:ConversationService 
               ,private autenticationService: AuthenticationService, private fbStorage:AngularFireStorage ) { 
    //this.friendId  = activatedRoute.snapshot.params['id'];
      this.autenticationService.getStatus().subscribe((session)=>{
        this.userService.getUserById(session.uid).valueChanges().subscribe((us: user)=>{
          this.user = us;
          //console.log(this.user);
          userService.getUserById(activatedRoute.snapshot.params['id']).valueChanges().subscribe(
            ( data: user)=>{ 
              this.friend = data;
              //console.log(this.friend);
              const ids = [this.user.id, this.friend.id].sort();
              this.conversation_id = ids.join('|'); // join une los elementos de un arreglo separados por el char que le indidque
              this.getConversations();
            },
            (error)=>{ console.log(error); });
        });
      })
  }
  ngOnInit() {
  }

  sendMessage(){
    const message={
      id: this.conversation_id,
      timestamp: Date.now(),
      text : this.textMessage,
      sender: this.user.id,
      receiver: this.friend.id,
      type : 'text'
    }
    this.conversationService.createConversation(message).then(()=>{
      this.textMessage = '';
    } )
  }

  sendZumbido(){
    const message={
      id: this.conversation_id,
      timestamp: Date.now(),
      text : '< zumbido >',
      sender: this.user.id,
      receiver: this.friend.id,
      type : 'zumbido'
    }
    this.conversationService.createConversation(message).then(()=>{});
    this.doZumbido();

  }

  doZumbido(){
    const audio = new Audio('assets/sound/zumbido.m4a');
    audio.play();
    this.shake = true; 

    setTimeout(() => {
      this.shake = false;
    }, 1000);

  }

  getConversations(){
    this.conversationService.getConversation(this.conversation_id).valueChanges().subscribe(
      (data)=>{
        this.conversation  =data ; 
        this.conversation.forEach((message)=>{
          if(!message.seen){
            message.seen = true;
            this.conversationService.editConversation(message);
            if(message.type == 'text'){              
              const audio = new Audio('assets/sound/new_message.m4a');
              audio.play();
            }
            else if(message.type == 'zumbido'){
              this.doZumbido();
            }
          }
        })

        
        setTimeout(function(){
          document.getElementById('chat').scrollTop = document.getElementById('chat').scrollHeight ; 
        },300);
      },
      (error)=>{console.log(error)}
    );    
  }

  getUserNick(id){
    if(id == this.friend.id){
      return this.friend.nick;
    }
    else{
      return this.user.nick;
    }
  }

  sendImage(){

    if(this.textMessage != null){
      this.sendMessage();
    }
    else{
      const imgID = Date.now();

      var message={
        id: this.conversation_id,
        timestamp: Date.now(),
        text : null,
        sender: this.user.id,
        receiver: this.friend.id,
        type : 'img',
        url : null
      }
  
      const pictur = this.fbStorage.ref('imgConversation/'+imgID+'.jpg').putString(this.croppedImage, 'data_url');
      pictur.then((result)=>{
        this.fbStorage.ref('imgConversation/'+imgID+'.jpg').getDownloadURL().subscribe((p)=>{
          message.url = p ; 
          this.conversationService.createConversation(message).then(()=>{
            const audio = new Audio('assets/sound/new_message.m4a');
            audio.play();
            this.croppedImage = null;  
            this.imageChangedEvent = null;
        }).catch((error)=>{
          alert('error al enviar imgagen');
          console.log(error)});
        });
      } ).catch((error)=> {console.log('error enviar imagen',error);})
    } 
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }
}
