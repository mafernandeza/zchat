import { Component, OnInit } from '@angular/core';
import { user } from '../Interfaces/user';
import { UserService } from '../Services/user.service';
import { AuthenticationService } from '../Services/authentication.service';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RequestsService } from '../Services/requests.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  friends : user[];
  user : user = null ;

  query : string = ""; 
  closeResult: string;
  friendEmail : string ;


  constructor(private userService : UserService, private authenticationService: AuthenticationService,
    private modalService: NgbModal, private requestsService: RequestsService) { 
    //let var1 : string  = "1" ; let var2 : string = "2"; 
    //let  var3 : boolean = true;
     this.userService.getUsers().valueChanges().subscribe((data: user[])=>{
      this.friends = data;
     },(error)=>{ console.log(error); });
     this.authenticationService.getStatus().subscribe(
      (data)=>{        
        userService.getUserById(data.uid).valueChanges().subscribe((data2: user)=>{
          this.user = data2;
          if(this.user.friends){
            this.user.friends = Object.values(this.user.friends);
          }
        }
        ,(error)=>{console.log(error);});
       },
      (error)=>{ console.log(error); });
  }

  ngOnInit() {
    //console.log('evento init ...(home)');
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  sendRequest(){
    const request = {
      timestamp : Date.now() ,
      receiver_email : this.friendEmail,
      sender: this.user.id,
      status: 'pending'
    }
    
    this.requestsService.createRequest(request).then(
      ()=>{ alert('solocitud enviada'); 
            document.getElementById('closeModalAddFriends').click();
          },
      (error)=>{ console.log(error); alert('ocurrio un error.'); });
  }
}
