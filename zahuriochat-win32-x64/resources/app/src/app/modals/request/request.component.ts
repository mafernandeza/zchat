import { Component, OnInit } from '@angular/core';
import {DialogComponent ,DialogService } from 'ng2-bootstrap-modal';
import { UserService } from 'src/app/Services/user.service';
import { RequestsService } from 'src/app/Services/requests.service';
import { user } from 'src/app/Interfaces/user';

export interface PromptModel {
  scope: any,
  currentRequest: any
}
@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent extends DialogComponent<PromptModel, any> implements PromptModel {
scope : any;
shouldAdd : string = 'Yes';
currentRequest: any;
user : user; 

  constructor(public dialogService: DialogService, private userService: UserService, private requestService: RequestsService ) {
    super(dialogService);

   }

   ngOnInit(){    
     this.userService.getUserById(this.currentRequest.sender).valueChanges().subscribe(
       (data:user)=>{
        this.user = data;
       }
     );
   }
   acept(){
     if(this.shouldAdd == 'Yes'){
       this.requestService.setRequestStatus(this.currentRequest,'accepted').then(
         (data)=>{
           console.log(data);
          this.userService.addFriend(this.scope.user.id, this.currentRequest.sender).then(
            ()=>{ alert('solicitud aceptada con exito');}
          );
         }
       ).catch(
         (error)=>{
           alert('ocurrio un error!');
           console.log(error);})
     }
     else if(this.shouldAdd == 'No'){
      this.requestService.setRequestStatus(this.currentRequest,'rejected').then(
        (data)=>{
          console.log(data);

        }
      ).catch(
        (error)=>{
          alert('ocurrio un error!');
          console.log(error);})
    }
    else if(this.shouldAdd == 'Later'){
      this.requestService.setRequestStatus(this.currentRequest,'decide_later').then(
        (data)=>{
          console.log(data);

        }
      ).catch(
        (error)=>{
          alert('ocurrio un error!');
          console.log(error);})
    }
   }
}
