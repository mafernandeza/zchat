import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private angularFireDataBase: AngularFireDatabase) { }

  createRequest(request){
    var cleanMail = request.receiver_email;
    var i = cleanMail.indexOf('.'); 
    //cleanMail = cleanMail.replace('.', ',');
    do{
      cleanMail = cleanMail.replace('.', ',');
      i = cleanMail.indexOf('.');
    }
    while(i>0);

    return this.angularFireDataBase.object('requests/'+cleanMail+'/'+ request.sender).set(request);
  }

  setRequestStatus(request, status){   
    var cleanMail = request.receiver_email;
    var i = cleanMail.indexOf('.'); 
    //cleanMail = cleanMail.replace('.', ',');
    do{
      cleanMail = cleanMail.replace('.', ',');
      i = cleanMail.indexOf('.');
    }
    while(i>0);

    return this.angularFireDataBase.object('requests/'+cleanMail+'/'+ request.sender+'/status').set(status);
  }

  getRequestsForEmail(email){
    var cleanMail = email;
    var i = cleanMail.indexOf('.'); 
    //cleanMail = cleanMail.replace('.', ',');
    do{
      cleanMail = cleanMail.replace('.', ',');
      i = cleanMail.indexOf('.');
    }
    while(i>0);
    return this.angularFireDataBase.list('requests/'+cleanMail);
  }
}
