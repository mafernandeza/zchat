import { Injectable } from '@angular/core';
import { user } from '../Interfaces/user';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private angularFireDataBase: AngularFireDatabase) { }

  getUsers(){
    return this.angularFireDataBase.list('/users');
  }
  getUserById(id){
    return this.angularFireDataBase.object('/users/'+id);
  }
  createUser(user: user){
    user.status = 'online';
    return this.angularFireDataBase.object('/users/'+user.id).set(user);
  }
  EditUser(user: user){
    return this.angularFireDataBase.object('/users/'+user.id).set(user);
  }
  setAvatar(avatar, id){
    return this.angularFireDataBase.object('/users/'+id+'/avatar').set(avatar);
  }

  addFriend(userId,friendId){
           this.angularFireDataBase.object('users/'+userId+'/friends/'+friendId).set(friendId);
    return this.angularFireDataBase.object('users/'+friendId+'/friends/'+userId).set(userId);
  }
}
