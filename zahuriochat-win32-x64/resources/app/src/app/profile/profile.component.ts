import { Component, OnInit } from '@angular/core';
import { user } from '../Interfaces/user';
import { UserService } from '../Services/user.service';
import { AuthenticationService } from '../Services/authentication.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';//antes era import { AngularFireStorage } from ‘angularfire2/storage’;




@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user : user = null;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  picture : any;
  cambiarImg : boolean =false;

  constructor( private userService: UserService, private authenticationService: AuthenticationService
    , private fbStorage:AngularFireStorage) {
    
    this.authenticationService.getStatus().subscribe(
     (data)=>{
       userService.getUserById(data.uid).valueChanges().subscribe((data2: user)=>{
         this.user = data2; 
         this.croppedImage = this.user.avatar;
       }
       ,(error)=>{console.log(error);});
      },
     (error)=>{ console.log(error); });
  }
  ngOnInit() {
  }
  saveSettings (){
    if(this.cambiarImg==true){
      const currentPictureID = Date.now();
      const pictures = this.fbStorage.ref('pictures/'+currentPictureID+'.jpg').putString(this.croppedImage, 'data_url');
      pictures.then((result)=>{
        this.picture = this.fbStorage.ref('pictures/'+currentPictureID+'.jpg').getDownloadURL().subscribe((p)=>{
          this.userService.setAvatar(p,this.user.id).then(()=>{alert('imagen subida correctamente');
        }).catch((error)=>{
          alert('error al subir imgagen');
          console.log(error)});
        });
      } ).catch((error)=> {console.log('err1',error);})
    }
    else{
   
      this.userService.EditUser(this.user).then(
        (result)=>{
          alert('cambios realizados');
        },(error)=> {console.log(error); });
    }
  }
  fileChangeEvent(event: any): void {
    this.cambiarImg =true;
    this.imageChangedEvent = event;
  }
imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
}
imageLoaded() {
    // show cropper
}
cropperReady() {
    // cropper ready
}
loadImageFailed() {
    // show message
}
}
