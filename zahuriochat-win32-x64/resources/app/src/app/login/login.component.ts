import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../Services/authentication.service';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  operation : string  = "login";
  email : string  = null; 
  password : string = null;
  nick: string = null;
  constructor(private autenticationService: AuthenticationService, private userService: UserService
    , private router: Router) { }

  ngOnInit() {
  }

  login(){
    this.autenticationService.loginWithEmail(this.email, this.password).then((data)=>{
      this.router.navigate(['home']);
    }).catch((error) => {
      alert('Error') ; 
      console.log(error);
    });
  }

  register(){
    this.autenticationService.registerWithEmail(this.email, this.password).then((data)=>{

      const user = {
        id : data.user.uid,
        email:this.email,
        nick: this.nick,
      }
      this.userService.createUser(user).then((data2)=>{
        console.log(data2);
        alert('guardado correctamente');
      }).catch((error2)=>{
        console.log(error2) ;
        alert('ocurrio un error al guardar en bd');
      });

      alert('log ok');
      console.log(data);
    }).catch((error) => {
      alert('Error') ; 
      console.log(error);
    });
  }
}
