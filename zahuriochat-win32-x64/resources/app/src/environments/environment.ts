// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBbV3e0wTWhqD8cV4y1n6JYxYAGNzYywvc",
    authDomain: "zhat-b2aba.firebaseapp.com",
    databaseURL: "https://zhat-b2aba.firebaseio.com",
    projectId: "zhat-b2aba",
    storageBucket: "zhat-b2aba.appspot.com",
    messagingSenderId: "278106215479",
    appId: "1:278106215479:web:eab08f2e0a061728fd5bb2",
    measurementId: "G-HMMEJM32CG"
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
